//
//  main.cpp
//  GLUTSkeleton
//
//  Created by Evan Chapman on 1/29/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include <iostream>
#include <GLUT/GLUT.h>


/*
 ====================
 Global Declarations
 ====================
 */
GLclampf        bgRed;
GLclampf        bgGreen;
GLclampf        bgBlue;
int             mouseX;
int             mouseY;
void            init();
void            display();
void            reshape(int h,int w);
void            mouse(int button,int state,int x,int y);
void            motion(int x,int y);
void            passiveMotion(int x,int y);


GLubyte fly[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x03, 0x80, 0x01, 0xC0, 0x06, 0xC0, 0x03, 0x60,
    0x04, 0x60, 0x06, 0x20, 0x04, 0x30, 0x0C, 0x20,
    0x04, 0x18, 0x18, 0x20, 0x04, 0x0C, 0x30, 0x20,
    0x04, 0x06, 0x60, 0x20, 0x44, 0x03, 0xC0, 0x22,
    0x44, 0x01, 0x80, 0x22, 0x44, 0x01, 0x80, 0x22,
    0x44, 0x01, 0x80, 0x22, 0x44, 0x01, 0x80, 0x22,
    0x44, 0x01, 0x80, 0x22, 0x44, 0x01, 0x80, 0x22,
    0x66, 0x01, 0x80, 0x66, 0x33, 0x01, 0x80, 0xCC,
    0x19, 0x81, 0x81, 0x98, 0x0C, 0xC1, 0x83, 0x30,
    0x07, 0xe1, 0x87, 0xe0, 0x03, 0x3f, 0xfc, 0xc0,
    0x03, 0x31, 0x8c, 0xc0, 0x03, 0x33, 0xcc, 0xc0,
    0x06, 0x64, 0x26, 0x60, 0x0c, 0xcc, 0x33, 0x30,
    0x18, 0xcc, 0x33, 0x18, 0x10, 0xc4, 0x23, 0x08,
    0x10, 0x63, 0xC6, 0x08, 0x10, 0x30, 0x0c, 0x08,
    0x10, 0x18, 0x18, 0x08, 0x10, 0x00, 0x00, 0x08};

/*
 =====================
 Main Entry
 -Application entry
 =====================
 */
int main(int argc,char ** argv){
    std::cout << "Initializing OpenGl...\n";
    
    //Initialize GLUT
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMainLoop();
    return 0;
}


/*
 =====================
 init
 -Prepare opengl for drawing
 =====================
 */
void init(){
    bgRed   =   0.0f;
    bgBlue  =   0.0f;
    bgGreen =   0.0f;
    glClearColor(bgRed,bgBlue,bgGreen,0.0);
    glShadeModel(GL_FLAT);
}


/*
 =====================
 display
 -perform opengl drawing
 =====================
 */
void display(){
    glClear(GL_COLOR_BUFFER_BIT);
    
    //Prepare opengl states
    glColor3f(1.0f, 1.0f, 1.0f);
    
    
    //Perform open gl draw between begin and end
    glEnable(GL_POLYGON_STIPPLE);
    glPolygonStipple(fly);
    glRectf(25.0f, 25.0f, 125.0f, 125.0f);

    
    //end opengl states
    //end state code here
    
    //Flush drawing to display
    glFlush();
}


/*
 =====================
 reshape
 -invalidate display and force redraw
 =====================
 */
void reshape(int w,int h){
    glViewport( 0 , 0 , (GLsizei) w , (GLsizei) h );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D( 0.0f, (GLdouble)w, 0.0f, (GLdouble) h );
}


/*
 =====================
 -mouse
 -callback when mouse status changes
 =====================
 */
void mouse(int button,int state, int x,int y){
    //Insert behavior
}


/*
 =====================
 -motion
 -callback when mouse moves
 =====================
 */

void motion(int x, int y){
    //insert behavior here
}

/*
 =====================
 -passiveMotion
 -callback when mouse moves
 =====================
 */

void passiveMotion(int x,int y){
    //insert behavior here
}
















































