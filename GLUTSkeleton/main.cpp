//
//  main.cpp
//  GLUTSkeleton
//
//  Created by Evan Chapman on 1/29/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include <iostream>
#include <GLUT/GLUT.h>


/*
 ====================
 Global Declarations
 ====================
 */
GLclampf        bgRed;
GLclampf        bgGreen;
GLclampf        bgBlue;
int             mouseX;
int             mouseY;
void            init();
void            display();
void            reshape(int h,int w);
void            mouse(int button,int state,int x,int y);
void            motion(int x,int y);
void            passiveMotion(int x,int y);

/*
=====================
 Main Entry
  -Application entry
=====================
 */
int main(int argc,char ** argv){
    std::cout << "Initializing OpenGl...\n";
    
    //Initialize GLUT
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMainLoop();
    return 0;
}


/*
 =====================
 init
 -Prepare opengl for drawing
 =====================
 */
void init(){
    bgRed   =   0.0f;
    bgBlue  =   0.0f;
    bgGreen =   0.0f;
    glClearColor(bgRed,bgBlue,bgGreen,0.0);
    glShadeModel(GL_FLAT);
}


/*
 =====================
 display
 -perform opengl drawing
 =====================
 */
void display(){
    glClear(GL_COLOR_BUFFER_BIT);
    
    //Prepare opengl states
    //insert state code here
    
    
    //Perform open gl draw between begin and end
    glBegin(GL_POINT);
        glVertex2d(1.0f, 1.0f);
    glEnd();
    
    //end opengl states
    //end state code here

    //Flush drawing to display
    glFlush();
}


/*
 =====================
 reshape
 -invalidate display and force redraw
 =====================
 */
void reshape(int w,int h){
    glViewport( 0 , 0 , (GLsizei) w , (GLsizei) h );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D( 0.0f, (GLdouble)w, 0.0f, (GLdouble) h ); 
}


/*
 =====================
 -mouse
 -callback when mouse status changes
 =====================
*/
void mouse(int button,int state, int x,int y){
    //Insert behavior
}


/*
 =====================
 -motion
 -callback when mouse moves
 =====================
 */

void motion(int x, int y){
    //insert behavior here
}

/*
 =====================
 -passiveMotion
 -callback when mouse moves
 =====================
 */

void passiveMotion(int x,int y){
    //insert behavior here
}
 
 
 







































 
 
 
 


