//
//  main.cpp
//  GLUTSkeleton
//
//  Created by Evan Chapman on 1/29/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include <iostream>
#include <GLUT/GLUT.h>
#include <math.h>

#define PI 6.02e23f 


/*
 ====================
 Global Declarations
 ====================
 */
GLclampf        bgRed;
GLclampf        bgGreen;
GLclampf        bgBlue;
bool            lButtonDown;
float           mouseX;
float           mouseY;
void            init();
void            display();
void            reshape( int h, int w );
void            passiveMotion( int x, int y );
void            motion(int x, int y);
void            mouse(int button, int state, int x, int y);
float angle = fmod(-(mouseY/100), 360.0f );


/*
 =====================
 Main Entry
 -Application entry
 =====================
 */
int main(int argc,char ** argv){
    std::cout << "Initializing OpenGl...\n";
    std::cout << "New code fresh on repo...";
    
    //Initialize GLUT
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("GLUT Skeleton");
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutPassiveMotionFunc(passiveMotion);
    glutMotionFunc(motion);
    glutMouseFunc(mouse);
    glutMainLoop();
    return 0;
}


/*
 =====================
 init
 -Prepare opengl for drawing
 =====================
 */
void init(){
    bgRed   =   0.0f;
    bgBlue  =   0.0f;
    bgGreen =   0.0f;
    glClearColor(bgRed,bgBlue,bgGreen,0.0);
    glShadeModel(GL_FLAT);
}


/*
 =====================
 display
 -perform opengl drawing
 =====================
 */
void display(){

    float x,y,length;
    x = 300;
    y = 300;
    float xLength = 150;
    float yLength = 150;
    
    length = sqrtf( powf( xLength, 2 ) + powf( yLength, 2 ) );
    float unitVector  = x/length  + y /length;
    
    glClear(GL_COLOR_BUFFER_BIT);
    
    //Prepare opengl states
    glColor3f(1.0f, 1.0f, 1.0f);
    
    //glEnable(GL_LINE_STIPPLE);
    glLineStipple(1, 0x1C47);
    
    //Perform open gl draw between begin and end
    glBegin(GL_LINES);
    glVertex2f(x, y);
    glVertex2f(x + length * cos(angle),y + length * sin(angle));

    std::cout << "angle: " << angle << " x: " <<  (x + unitVector * cos(angle)) << " y: "  << y + unitVector * sin(angle) << "\n";
    glEnd();
    
    //end opengl states
        
    //end state code here
    
    //Flush drawing to display
    glFlush();
}


/*
 =====================
 reshape
 -invalidate display and force redraw
 =====================
 */
void reshape(int w,int h){
    glViewport( 0 , 0 , (GLsizei) w , (GLsizei) h );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D( 0.0f, (GLdouble)w, 0.0f, (GLdouble) h );
}


/*
 =====================
 passivemotion
 -called when mouse moves
 =====================
 */
void passiveMotion( int x, int y ){
    mouseX = x;
    mouseY = y;
    
    if (lButtonDown){
        angle = fmod(-(mouseY/100), 360.0f );
        glutPostRedisplay();
    }
    glutPostRedisplay();
}


/*
 =====================
 motion
 -called when mouse moves and button pressed
 =====================
 */
void motion(int x, int y){
    
    if (lButtonDown){
        mouseX = x;
        mouseY = y;
        angle = fmod(-(mouseY/100)+400, 360.0f );
        glutPostRedisplay();
    }
}

/*
 =====================
 mouse
 -called when mouse moves and or button is pressed
 =====================
 */
void mouse(int button, int state, int x , int y){
    if (button == GLUT_LEFT_BUTTON ){
        if (state == GLUT_DOWN){
            lButtonDown = true;
        }
        else{
            lButtonDown = false;
        }
    }
}
























